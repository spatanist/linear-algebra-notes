\section{Eigensystems}

\subsection{PageRank algorithm}

The original algorithm that Google used to rank web pages is
``PageRank'' that assigns for each webpage $i$ a ranking number
$P(i)$.  The pagerank $P(i)$ of $i$ represents how ``popular'' the
page $i$ is by counting the number of pages that link to $i$ weighted
by the popularity of these pages.  Consider as an example a system
with 4 web pages that link to each other as in the following figure.

\vspace{0.1in}
\begin{tikzpicture}[scale=2]
  \Vertex[x=0,y=0]{1} 
  \Vertex[x=1,y=0]{2} 
  \Vertex[x=0,y=1]{3} 
  \Vertex[x=1,y=1]{4}
  \Edge[style={bend right, ->}](1)(2)
  \Edge[style={bend left, ->}](1)(4)
  \Edge[style={bend left, ->}](3)(4)
  \Edge[style={bend right, ->}](2)(4)
  \Edge[style={bend right, ->}](2)(3)
  \Edge[style={bend right, ->}](2)(1)
\end{tikzpicture}
\vspace{0.1in}

Consider web page $4$.  Ideally, we would like to have
\[
P(4) = P(1)/2 + P(2)/3 + P(3),
\]
because web page 1 links to 2 pages so we have to divide its influence
by 2, similarly, and web page 2 links to 3 pages, so we have to divide
$P(2)$ by 3.  You can also write down the requirement for each other
vertex:
\[
P(1) = P(2)/3,
\]
\[
P(2) = P(1)/2,
\]
and
\[
P(3) = P(2)/3.
\]
How can we find the page rank $P$ that satisfies these requirements?
We can do that iteratively, i.e., we start with some arbitary page
rank value $P_0$.  We then update the page rank to get $P_1,P_2,$ and
so on, using the following equations:
\begin{eqnarray*}
  P_{i+1}(1) & = & P_i(2)/3 \\
  P_{i+1}(2) & = & P_i(1)/2 \\
  P_{i+1}(3) & = & P_i(2)/3 \\
  P_{i+1}(4) & = & P_i(1)/2 + P_i(2)/3 + P_i(3)
\end{eqnarray*}

Or, in the matrix form,
\[
\begin{bmatrix}
  P_{i+1}(1) &
  P_{i+1}(2) &
  P_{i+1}(3) &
  P_{i+1}(4) 
\end{bmatrix}
=
\begin{bmatrix}
  P_i(1) &
  P_i(2) &
  P_i(3) &
  P_i(4) 
\end{bmatrix}
\begin{bmatrix}
  0 & 1/3 & 0 & 0 \\
  1/2 & 0 & 0 & 0 \\
  0 & 1/3 & 0 & 0 \\
  1/2 & 1/3 & 1 & 0
\end{bmatrix}
\]
Let's denote the matrix as $A$, i.e., let 
\[
A =
\begin{bmatrix}
  0 & 1/3 & 0 & 0 \\
  1/2 & 0 & 0 & 0 \\
  0 & 1/3 & 0 & 0 \\
  1/2 & 1/3 & 1 & 0
\end{bmatrix}
\]
Also write $P_i$ as a 4-vector (as row vector).  We have that
\[
P_{i+1}=P_{i}A.
\]
Applying the same eqation many times, we get
\[
P_{i+1} = P_{i}A = P_{i-1}AA = P_{i+2}AAA = \cdots,
\]
or equivalently, if we start with $P_0$, we have that
\[
P_i = P_0 A^i.
\]
So starting with $P_0$, we repeatedly multiply $P_i$ with $A$ to get
$P_{i+1}$.  We hope that at some point, the result converges to some
vector.  How would we analyze this procedure?

Remarks: The actual PageRank algorithm uses a slightly different
matrix $A$ that includes small probability for each web page to jump
to every other web pages.  This essentially ensures that the PageRank
vector $P$ converges.

\subsection{Random walks}

We will look at a slightly different random process called ``random
walks'' on an undirected graph.  The process that works in rounds can
be described as follows.  Imaging a player locates at some vertex in
the graph.  For each round, the player has some probability (say
$1/2$) to stay at the same vertex so she toss a fair coin.  If the
coin turns up head, she stays at the same vertex.  Otherwise, if the
coin comes up tail, she picks randomly uniformly an adjacent vertex to
move to.

Consider the following graph.

\vspace{0.1in}
\begin{tikzpicture}[scale=2]
  \Vertex[x=0,y=0]{1} 
  \Vertex[x=1,y=0]{2} 
  \Vertex[x=0,y=1]{3} 
  \Vertex[x=1,y=1]{4}
  \Edge[style={bend right}](1)(2)
  \Edge[style={bend left}](1)(4)
  \Edge[style={bend left}](3)(4)
  \Edge[style={bend right}](2)(4)
  \Edge[style={bend right}](2)(3)
\end{tikzpicture}
\vspace{0.1in}

For time $t$ and vertex $i$, we let $P_t(i)$ denote the probability
that the player is at vertex $i$ at time $t$.

If initially the player starts at vertex 1, the probability $P_0$ is
the following vector
\[
P_0 =
\begin{bmatrix}
  1 &
  0 &
  0 &
  0
\end{bmatrix}
\]
The matrix that represents the random move is
\[
A=
\begin{bmatrix}
  1/2 & 1/4 & 0 & 1/4 \\
  1/6 & 1/2 & 1/6 & 1/6 \\
  0 & 1/4 & 1/2 & 1/4 \\
  1/6 & 1/6 & 1/6 & 1/2
\end{bmatrix}
\]
We call $A$ the {\em transition matrix}.  The probability $P_1$ that
after the first step the player is at each vertex is
\[
P_1=P_0A=
\begin{bmatrix}
  1 &
  0 &
  0 &
  0
\end{bmatrix}
\begin{bmatrix}
  1/2 & 1/4 & 0 & 1/4 \\
  1/6 & 1/2 & 1/6 & 1/6 \\
  0 & 1/4 & 1/2 & 1/4 \\
  1/6 & 1/6 & 1/6 & 1/2
\end{bmatrix}
=
\begin{bmatrix}
  1/2 &
  1/4 &
  0 &
  1/4
\end{bmatrix}
\]

To save space, we sometimes write a row vector as a transpose of a
column vector.  The probability $P_2$ that after the first step the
player is at each vertex is
\[
P_2=P_1A=
\begin{bmatrix}
  1/2 \\
  1/4 \\
  0 \\
  1/4
\end{bmatrix}^T
\begin{bmatrix}
  1/2 & 1/4 & 0 & 1/4 \\
  1/6 & 1/2 & 1/6 & 1/6 \\
  0 & 1/4 & 1/2 & 1/4 \\
  1/6 & 1/6 & 1/6 & 1/2
\end{bmatrix}
=
(1/2)
\begin{bmatrix}
  1/2 \\
  1/4 \\
  0 \\
  1/4
\end{bmatrix}^T +
(1/4)
\begin{bmatrix}
  1/6 \\
  1/2 \\
  1/6 \\
  1/6
\end{bmatrix}^T +
(1/4)
\begin{bmatrix}
  1/6 \\
  1/6 \\
  1/6 \\
  1/2
\end{bmatrix}^T
=
\begin{bmatrix}
  1/3 \\
  7/24 \\
  1/12 \\
  7/24
\end{bmatrix}^T
\]
We can repeatedly perform this calculation for many times.  We do so
numerically.  After 5 multiplications, we get that
\[
P_5 \approx
\begin{bmatrix}
0.21566358 &  0.29996142 & 0.18441358 & 0.29996142
\end{bmatrix}
\]
After 10 multiplications, we have
\[
P_{10} \approx
\begin{bmatrix}
  0.20048829 & 0.3 & 0.19951172 & 0.3      
\end{bmatrix}
\]
And after 100 multiplications, we have
\[
P_{100} \approx
\begin{bmatrix}
  0.2 & 0.3 & 0.2 & 0.3      
\end{bmatrix},
\]
i.e., it eventually converges.  Suppose that vector $\vect{p}$ is the
final converged vector.  It has to satisfy
\[
\vect{p}^T = \vect{p}^T A.
\]
If we think of matrix multiplication as an operation that a matrix
performs on a vector, the above equation states that $A$ does not
change $\vect{p}$.  We sometimes relax the constraint so that we want
to find vectors that $A$ does not change their ``directions'', i.e.,
we are interested in vectors $\vv$ such that
\[
\lambda\vv^T = \vv^T A.
\]
We call these vectors {\em eigenvectors} (more precisely the left
eigenvectors), and the scalar $\lambda$ {\em eigenvalues}.

\subsection{Definitions}

 
